# Wallet-issuer Chart


## Configuration

The following tables lists the configurable parameters of the wallet-issuer chart and their default values.

|           Parameter           |                Description                       |           Default                 |
|-------------------------------|--------------------------------------------------|-----------------------------------|
| `image.repository`            | issuer image                                     | `gcr.io/mc-digital-native/issuer` |
| `image.PullPolicy`            | Image pull policy                                | `IfNotPresent`                    |
| `image.tag`                   | Image tag                                        | `master`                          |